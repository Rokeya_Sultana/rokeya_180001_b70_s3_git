 Command line instructions
Git global setup

git config --global user.name "Rokeya Sultana"
git config --global user.email "dupsatar54@gmail.com"

Create a new repository

git clone https://gitlab.com/Rokeya_Sultana/rokeya_180001_b70_s3_git.git
cd rokeya_180001_b70_s3_git
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://gitlab.com/Rokeya_Sultana/rokeya_180001_b70_s3_git.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://gitlab.com/Rokeya_Sultana/rokeya_180001_b70_s3_git.git
git push -u origin --all
git push -u origin --tags
